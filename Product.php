<?php

class Product
{

    /**
    * @var string
    */
    private $sku;

    /**
    * @var float
    */
    private $cost;

    /**
    * @var float
    */
    private $price;

    /**
    * @var integer
    */
    private $qty;

    /**
    * Create a new Product
    *
    * @param string $sku
    * @param float $cost
    * @param float $price
    * @param integer $qty
    * @return void
    */
    public function __construct($sku, $cost, $price, $qty)
    {
        $this->sku = $sku;
        $this->cost = $cost;
        $this->price = $price;
        $this->qty = $qty;
    }

    /**
    * Get the private attributes
    *
    * @param string $key
    * @return mixed
    */
    public function __get($key)
    {
        if (property_exists($this, $key)) {
            return $this->$key;
        }
    }

    /**
    * Get the profit margin
    *
    * @return float
    */
    public function getProfitMargin()
    {
        return $this->price - $this->cost;
    }

    /**
    * Get the total profit
    * @param float $rate
    * @return float
    */
    public function getTotalProfit($rate=1)
    {
        $totalProfit = $this->qty * $this->getProfitMargin();
        if ($rate) $totalProfit *= $rate;
        return $totalProfit;
    }

}
