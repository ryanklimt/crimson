<?php
/*
ToDo:
- Any file type uploaded is attempted to be processed and throws lots of errors. [x]
- API call is done "repeatedly" for each product and column that uses it, not efficient [x]
- A little too much HTML rendered within the object. [x]
*/
ini_set("display_errors", 1);
ini_set("display_startup_errors", 1);
error_reporting(E_ALL);
if (isset($_FILES) && isset($_FILES["csvFile"])) {
  require_once "Csv.php";
  require_once "Collection.php";
  require_once "Product.php";
  require_once "Formatter.php";
  $csv = new Csv($_FILES["csvFile"]);
  $collection = new Collection();
  $collection->loadCsv($csv);
}
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="PHP Exercise – File Processor">
    <meta name="author" content="Ryan Klimt">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <title>PHP Exercise – File Processor</title>
  </head>
  <body>
    <section class="container">
      <h1 class="mt-5 mb-5 text-center">PHP Exercise – File Processor</h1>
      <?php if (isset($collection)) { ?>
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <?php
                $cols = ["SKU", "Cost", "Price", "QTY", "Profit Margin", "Total Profit (USD)", "Total Profit (CAD)",];
                foreach ($cols as $col) {
                  echo '<th scope="col">' . $col . "</th>";
                }
              ?>
            </tr>
          </thead>
          <tbody>
            <?php if ($collection) {
              foreach ($collection->products as $product) { ?>
                <tr>
                  <?php
                    $collection->render($product->sku, "string");
                    $collection->render($product->cost, "currency");
                    $collection->render($product->price, "currency");
                    $collection->render($product->qty, "integer");
                    $collection->render($product->getProfitMargin(), "currency");
                    $collection->render($product->getTotalProfit(), "currency");
                    $collection->render($product->getTotalProfit($collection->cad_usd_rate), "currency");
                  ?>
                </tr>
              <?php } ?>
              <tr>
                <td>
                  <strong>Summary</strong>
                </td>
                <?php
                  $collection->render($collection->getAverage('cost'), "currency");
                  $collection->render($collection->getAverage('price'), "currency");
                  $collection->render($collection->getTotal('qty'), "integer");
                  $collection->render($collection->getAverageProfitMargin(),"currency");
                  $collection->render($collection->getTotalProfit(), "currency");
                  $collection->render($collection->getTotalProfit($collection->cad_usd_rate),"currency");
                ?>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      <?php } else { ?>
        <form enctype="multipart/form-data" action="" method="POST">
          <div class="form-group mb-3">
            <label for="csvFile">Select csv file</label>
            <input type="file" class="form-control-file" id="csvFile" name="csvFile" accept=".csv">
          </div>
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      <?php } ?>
    </section>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
  </body>
</html>
