<?php

class Csv
{

    private $_file;

    public function __construct($file)
    {
        $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
        if ($ext !== 'csv') die('Invalid CSV: File must have .csv extension.');
        if ($file['size'] > 2000000) die('Invalid CSV: File size is too large.');
        $this->_file = fopen($file['tmp_name'], "r");
    }

    public function closeCsv()
    {
        fclose($this->_file);
    }

    public function readCsv()
    {
        return fgetcsv($this->_file);
    }

}
