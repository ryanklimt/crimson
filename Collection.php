<?php

class Collection
{

    /**
    * @var array
    */
    private $products;

    /**
    * @var float
    */
    public $cad_usd_rate;

    /**
    * Create a new collection
    *
    * @return void
    */
    public function __construct(array $products = [])
    {
        $this->products = $products;
        $this->cad_usd_rate = $this->lookupCADConversion();
    }

    /**
    * Get the private attributes
    *
    * @param Csv $csv
    * @return void
    */
    public function loadCsv($csv)
    {
        $header = array();
        while (($row = $csv->readCsv()) !== false) {
            if ($header) {
                $product = array_combine($header, $row);
                $this->addProduct($product['sku'], $product['cost'], $product['price'], $product['qty']);
            } else {
                $header = $row;
            }
        }
        $csv->closeCsv();
    }

    /**
    * Get the private attributes
    *
    * @param string $key
    * @return mixed
    */
    public function __get($key)
    {
        if (property_exists($this, $key)) {
            return $this->$key;
        }
    }

    /**
    * Add a product to array of products
    *
    * @param string $sku
    * @param float $cost
    * @param float $price
    * @param integer $qty
    * @return void
    */
    public function addProduct($sku, $cost, $price, $qty)
    {
        $this->products[] = new Product($sku, $cost, $price, $qty);
    }

    /**
    * Count the number of products
    *
    * @return int
    */
    public function count()
    {
        return count($this->products);
    }

    /**
    * Get the total for given column
    * @param string $column
    * @return mixed
    */
    public function getTotal($column)
    {
      $value = 0;
      foreach ($this->products as $product)
          $value += $product->$column;
      return $value;
    }

    /**
    * Get the total profit
    * @param float $rate
    * @return float
    */
    public function getTotalProfit($rate=1)
    {
        $value = 0;
        foreach ($this->products as $product)
            $value += $product->getTotalProfit();
        if ($rate) $value *= $rate;
        return $value;
    }

    /**
    * Get the average for given column
    * @param string $column
    * @return float
    */
    public function getAverage($column)
    {
      return $this->getTotal($column) / $this->count();
    }

    /**
    * Get the average profit margin
    *
    * @return float
    */
    public function getAverageProfitMargin()
    {
        $value = 0;
        foreach ($this->products as $product)
            $value += $product->getProfitMargin();
        return $value / $this->count();
    }

    /**
    * Get the current USD to CAD conversion
    *
    * @return float
    */
    public function lookupCADConversion()
    {
          $params = array(
              'access_key' => 'dddcd9d85b8396ef318022ca459f148a',
              'format' => 'json',
              'symbols' => 'USD,CAD'
          );
          $base_url = 'http://data.fixer.io/api/latest';
          $api_url = $base_url . '?' . http_build_query($params);
          $json = file_get_contents($api_url);
          //$json = '{"success":true,"timestamp":1622861944,"base":"EUR","date":"2021-06-05","rates":{"USD":1.216655,"CAD":1.469987}}';
          $json = json_decode($json);
          return $json->rates->CAD / $json->rates->USD;
    }

    /**
    * Render html for product data
    *
    * @param mixed $value
    * @param string $type
    * @return void
    */
    public function render($value, $type)
    {
        $productData = new Formatter($value, $type);
        $productData->render();
    }

}
