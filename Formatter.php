<?php

class Formatter
{

    /**
    * @var mixed
    */
    private $value;

    /**
    * @var string
    */
    private $type;

    /**
    * Create a new Formatter
    *
    * @param mixed $value
    * @param string $type
    * @return void
    */
    public function __construct($value, $type='')
    {
        $this->value = $value;
        $this->type = $type;
    }

    /**
    * Get the private attributes
    *
    * @param string $key
    * @return mixed
    */
    public function __get($key)
    {
        if (property_exists($this, $key)) {
            return $this->$key;
        }
    }

    /**
    * Get the class for negative/positive values
    *
    * @return string
    */
    public function isNegative()
    {
        if ($this->type != 'currency' && $this->type != 'integer') return;
        return ($this->value >= 0) ? ' class="text-success"' : ' class="text-danger"';
    }

    /**
    * Get properly formatted values
    *
    * @return string
    */
    public function prettyData()
    {
        if ($this->type == 'currency') return '$' . number_format($this->value, 2);
        if ($this->type == 'integer') return number_format($this->value);
        return $this->value;
    }

    /**
    * Render html for product data
    *
    * @return void
    */
    public function render()
    {
        echo ('<td' . $this->isNegative() . '>' . $this->prettyData() . '</td>');
    }

}
